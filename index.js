class Cat {
  constructor(energy, hunger, sleep) {
    this.energy = 10;
    this.hunger = 0;
    this.sleeping = false;
  }

  play() {
    //if cat is sleeping you cant do anything
    if (this.sleeping) {
      thoughts.innerHTML = "Don't bother me when I sleep..";
      console.log("Don't bother me when I sleep.");
    } else {
      //if not sleeping does all the playing things
      if (this.energy >= 2 && this.hunger <= 8) {
        this.energy -= 2;
        this.hunger += 2;

        img[0].setAttribute("src", "imgs/playing.jpg");
        thoughts.innerHTML = "We're playing";
        setTimeout(function () {
          img[0].setAttribute("src", "imgs/cat.jpg");
          thoughts.innerHTML = "";
        }, 2000);
        console.log("We're playing", cat);
      } else {
        thoughts.innerHTML = "Too tired...";

        setTimeout(function () {
          thoughts.innerHTML = "";
        }, 2000);

        console.log("Too tired...");
      }
    }
  }

  eat() {
    if (this.sleeping) {
      thoughts.innerHTML = "Don't bother me when I sleep..";
      console.log("Don't bother me when I sleep.");
    } else {
      if (this.hunger >= 1) {
        this.energy += 1;
        if (this.energy > 10) {
          this.energy = 10;
        }
        this.hunger -= 1;

        img[0].setAttribute("src", "imgs/eating.jpg");
        thoughts.innerHTML = "Tasty..";
        setTimeout(function () {
          img[0].setAttribute("src", "imgs/cat.jpg");
          thoughts.innerHTML = "";
        }, 2000);

        console.log("Tasty..", cat);
      } else if (this.hunger == 0) {
        thoughts.innerHTML = "Not hungry";

        setTimeout(function () {
          thoughts.innerHTML = "";
        }, 2000);
        console.log("Not hungry");
      }
    }
  }

  speak() {
    if (this.sleeping) {
      thoughts.innerHTML = "Don't bother me when I sleep..";
      console.log("Don't bother me when I sleep.");
    } else {
      if (this.hunger <= 9) {
        this.hunger += 1;

        img[0].setAttribute("src", "imgs/barking.jpg");
        thoughts.innerHTML = "Meow- I mean, WOOF. Also 9/11 was an inside job.";
        setTimeout(function () {
          img[0].setAttribute("src", "imgs/cat.jpg");
          thoughts.innerHTML = "";
        }, 3000);

        console.log("Meow- I mean, WOOF.", cat);
      } else {
        thoughts.innerHTML =
          "If you want to talk some more feel free to talk to my lawyer. Or get me some food.";

        setTimeout(function () {
          thoughts.innerHTML = "";
        }, 3000);
        console.log(
          "If you want to talk some more feel free to talk to my lawyer. Or get me some food."
        );
      }
    }
  }

  sleep() {
    if (this.sleeping) {
      thoughts.innerHTML = "I can't sleep while I'm sleeping.";
      console.log("I can't sleep while I'm sleeping.");
    } else {
      if (this.energy <= 7 && this.hunger <= 5) {
        this.energy += 5;
        if (this.energy > 10) {
          this.energy = 10;
        }
        this.hunger += 2;
        this.sleeping = true;

        img[0].setAttribute("src", "imgs/sleeping.jpg");
        thoughts.innerHTML = "PurrrzzzZZZzzzzz";

        console.log("PurrrzzzZZZzzzzz", cat);
      } else {
        thoughts.innerHTML = "Not sleepy. I want blood.";
        setTimeout(function () {
          thoughts.innerHTML = "";
        }, 1000);
        console.log("Not sleepy. I want blood.");
      }
    }
  }

  wake() {
    this.sleeping = false;

    img[0].setAttribute("src", "imgs/waking.jpg");
    thoughts.innerHTML = "OKAY okay... I'm up.. so what's up.";
    setTimeout(function () {
      img[0].setAttribute("src", "imgs/cat.jpg");
    }, 1000);

    console.log("OKAY okay... I'm up.. so what's up.");
  }

  //logic for energy and hunger guages
  manageEnergy() {
    if (this.energy == 10) {
      energyDOM.style.height = "200px";
      energyNumberDOM.innerHTML = this.energy;
    } else if (this.energy == 9) {
      energyDOM.style.height = "180px";
      energyNumberDOM.innerHTML = this.energy;
    } else if (this.energy == 8) {
      energyDOM.style.height = "160px";
      energyNumberDOM.innerHTML = this.energy;
    } else if (this.energy == 7) {
      energyDOM.style.height = "140px";
      energyNumberDOM.innerHTML = this.energy;
    } else if (this.energy == 6) {
      energyDOM.style.height = "120px";
      energyNumberDOM.innerHTML = this.energy;
    } else if (this.energy == 5) {
      energyDOM.style.height = "100px";
      energyNumberDOM.innerHTML = this.energy;
    } else if (this.energy == 4) {
      energyDOM.style.height = "80px";
      energyNumberDOM.innerHTML = this.energy;
    } else if (this.energy == 3) {
      energyDOM.style.height = "60px";
      energyNumberDOM.innerHTML = this.energy;
    } else if (this.energy == 2) {
      energyDOM.style.height = "40px";
      energyNumberDOM.innerHTML = this.energy;
    } else if (this.energy == 1) {
      energyDOM.style.height = "20px";
      energyNumberDOM.innerHTML = this.energy;
    } else if (this.energy == 0) {
      energyDOM.style.height = "0px";
      energyNumberDOM.innerHTML = this.energy;
    }
  }

  manageHunger() {
    if (this.hunger == 10) {
      hungerDOM.style.height = "200px";
      hungerNumberDOM.innerHTML = this.hunger;
    } else if (this.hunger == 9) {
      hungerDOM.style.height = "180px";
      hungerNumberDOM.innerHTML = this.hunger;
    } else if (this.hunger == 8) {
      hungerDOM.style.height = "160px";
      hungerNumberDOM.innerHTML = this.hunger;
    } else if (this.hunger == 7) {
      hungerDOM.style.height = "140px";
      hungerNumberDOM.innerHTML = this.hunger;
    } else if (this.hunger == 6) {
      hungerDOM.style.height = "120px";
      hungerNumberDOM.innerHTML = this.hunger;
    } else if (this.hunger == 5) {
      hungerDOM.style.height = "100px";
      hungerNumberDOM.innerHTML = this.hunger;
    } else if (this.hunger == 4) {
      hungerDOM.style.height = "80px";
      hungerNumberDOM.innerHTML = this.hunger;
    } else if (this.hunger == 3) {
      hungerDOM.style.height = "60px";
      hungerNumberDOM.innerHTML = this.hunger;
    } else if (this.hunger == 2) {
      hungerDOM.style.height = "40px";
      hungerNumberDOM.innerHTML = this.hunger;
    } else if (this.hunger == 1) {
      hungerDOM.style.height = "20px";
      hungerNumberDOM.innerHTML = this.hunger;
    } else if (this.hunger == 0) {
      hungerDOM.style.height = "0px";
      hungerNumberDOM.innerHTML = this.hunger;
    }
  }
}

let cat = undefined;
let img = document.getElementsByTagName("img");

//creating object from class Cat
let createCatBtn = document.getElementById("createCat");

createCatBtn.addEventListener("click", function (e) {
  cat = new Cat();
  img[0].setAttribute("src", "imgs/cat.jpg");
  energyDOM.style.height = "200px";
  hungerDOM.style.height = "0px";
  energyNumberDOM.innerHTML = "10";
  hungerNumberDOM.innerHTML = "0";
  andHeSez.style.visibility = "visible";
  thoughts.innerHTML = "Hello !";
  for (let i = 1; i < buttons.length; i++) {
    buttons[i].style.visibility = "visible";
  }
  console.log(cat);
});

//playing with cat
let playCatBtn = document.getElementById("playCat");

playCatBtn.addEventListener("click", function (e) {
  cat.play();
  cat.manageEnergy();
  cat.manageHunger();
});

//making cat fat
let feedCatBtn = document.getElementById("feedCat");

feedCatBtn.addEventListener("click", function (e) {
  cat.eat();
  cat.manageEnergy();
  cat.manageHunger();
});

//making cat spill the beans on the undercover operation in Afghanistan
let talkCatBtn = document.getElementById("talkCat");

talkCatBtn.addEventListener("click", function (e) {
  cat.speak();
  cat.manageHunger();
});

//make the beast slumber
let sleepCatBtn = document.getElementById("sleepCat");

sleepCatBtn.addEventListener("click", function (e) {
  cat.sleep();
  cat.manageEnergy();
  cat.manageHunger();
});

//wake the beast from slumber
let wakeCatBtn = document.getElementById("wakeCat");

wakeCatBtn.addEventListener("click", function (e) {
  cat.wake();
});

// getting the needed stuff
let energyDOM = document.getElementById("energy");
let hungerDOM = document.getElementById("hunger");

let andHeSez = document.getElementById("andHeSez");
let thoughts = document.getElementById("felineThoughts");

let energyNumberDOM = document.getElementById("energyNumber");
let hungerNumberDOM = document.getElementById("hungerNumber");

let buttons = document.querySelectorAll("button");

console.log(buttons);
